#!/usr/bin/python3
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import os
import re
import sys
import yaml
import json

envvar_name_projectpath = 'OPENDESK_DEPLOYMENT_AUTOMATION_PATH' # name of the env var containing the path to the project directory

project_path = os.getenv(envvar_name_projectpath)

if project_path is None:
    print(f'! ERROR: The required environment variable {envvar_name_projectpath} was not set.')
    sys.exit()

project_file_charts = project_path+'/helmfile/environments/default/charts.yaml'
project_file_images = project_path+'/helmfile/environments/default/images.yaml'

dirname_build_artefacts = 'build_artefacts' # directory the build artefacts are placed into
filename_chart_index = 'chart-index.json'
filename_image_index = 'image-index.json'

image_index = {}
chart_index = {}

##
## Functions
##
def read_yaml(fqfn, required_element = None):
    fqfn_tmp = fqfn+".temp"
    print(f"- Reading/processing: {fqfn}")
    try:
        file_in = open(fqfn,'r')
        file_in_content = file_in.read()
        file_in.close()
        # replace a templating value with a dummy value
        file_out_content = re.sub(r': \{\{.+?\}\}', ': "templating_removed_by_asset_generator"', file_in_content)
        # remove additional (single) line templating completely
        file_out_content = re.sub(r'\{\{-.+?\}\}', '', file_out_content)
        # replace `# @supplier:` with `supplier` to process it yaml-style
        file_out_content = re.sub(r'# @supplier', 'supplier', file_out_content)
        file_out = open(fqfn_tmp, 'w')
        file_out.write(file_out_content)
        file_out.close()

        with open(fqfn_tmp, 'r') as file:
            yaml_all = yaml.safe_load_all(file)
            for y in yaml_all:
                if required_element in y:
                    return y
            print(f"! ERROR: Did not find {required_element} in {fqfn_tmp}")
            sys.exit()

    except OSError as e:
        print(f"! ERROR: Unable to open {fqfn}: {e}", file=sys.stderr)
        sys.exit()

##
## Main program
##
print(f"- working on project_path '{project_path}'")

if not os.path.exists(dirname_build_artefacts):
   os.mkdir(dirname_build_artefacts)

# process images
images_dict = read_yaml(project_file_images, 'images')
for image in images_dict["images"]:
    for image_attr in images_dict["images"][image]:
        if image_index.get(image) is None:
            image_index[image] = {}
        if not image_attr in {'registry', 'repository', 'tag', 'supplier'}:
            print(f"! ERROR: Unsupported attribute '{image_attr}' for f{image} in image-file '{project_file_images}'")
        else:
            image_index[image][image_attr] = images_dict["images"][image][image_attr]

# output image-index.json
with open(dirname_build_artefacts+'/'+filename_image_index, "w") as write_file:
    write_file.write(json.dumps(image_index, indent=2))

# process charts
charts_dict = read_yaml(project_file_charts, 'charts')
chart_index = {}
version_pinning_error = False
for chart_name, data in charts_dict['charts'].items():
    chart_index[chart_name] = {}
    chart_index[chart_name]['repo'] = 'oci://'+data['registry']+'/'+data['repository']
    chart_index[chart_name]['chart'] = data['name']
    chart_index[chart_name]['version'] = data['version']
    chart_index[chart_name]['verifiable'] = True if 'verify' in data and data['verify'] else False

# output chart-index.json
with open(dirname_build_artefacts+'/'+filename_chart_index, "w") as write_file:
    write_file.write(json.dumps(chart_index, indent=2))

# # download and tar-gzip charts
# dirname_helm_charts = "opendesk-helmchart-bundle" # will also be used for the name of the helm chart .tgz
# repo_status = {}
# if not os.path.exists(dirname_helm_charts):
#    os.mkdir(dirname_helm_charts)
# for name, values in chart_index.items():
#     if repo_status.get(values["repo"]) is None and values["oci"] is None:
#         os.system(f"helm repo add {name} {values['repo']}")
#         os.system(f"helm repo update {name}")
#         repo_status[values["repo"]] = True
#     if values["oci"]:
#         os.system(f"helm pull {values['repo']}/{values['chart']} --version {values['version']} -d ./{dirname_helm_charts}")
#     else:
#         os.system(f"helm pull {name}/{values['chart']} --version {values['version']} -d ./{dirname_helm_charts}")
# os.system(f"tar -zcvf {dirname_build_artefacts}/{dirname_helm_charts}.tgz {dirname_helm_charts}/*.tgz")
